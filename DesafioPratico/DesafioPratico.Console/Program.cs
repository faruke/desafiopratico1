﻿using DesafioPratico.NumeroParaTexto;
using System;
using System.Linq;

namespace DesafioPratico.Console {
    class Program {
        static void Main(string[] args) {

            if (args == null) throw new ArgumentNullException();
            if (args.Length < 2) throw new ArgumentException("Deve ser informado dois parâmetros.");
            if (args.Any(a => a.Any(b => !char.IsDigit(b)))) throw new ArgumentException("Somente números devem ser informados.");

            var numeroInicial = Convert.ToInt32(args[0]);
            var numeroFinal = Convert.ToInt32(args[1]);

            var processamentoDeNumeros = new ProcessamentoDeNumeros(numeroInicial, numeroFinal);
            
            System.Console.WriteLine(processamentoDeNumeros.NumerosPorExtensoUtilizados);
            
            System.Console.WriteLine($"{processamentoDeNumeros.NumerosUtilizados} = {processamentoDeNumeros.QuantidadeDeLetrasTotais}");

            System.Console.ReadKey();
        }
    }
}
