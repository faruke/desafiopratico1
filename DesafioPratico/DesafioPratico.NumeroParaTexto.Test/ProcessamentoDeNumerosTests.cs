using System;
using System.Linq;
using Xunit;

namespace DesafioPratico.NumeroParaTexto.Test {
    public class ProcessamentoDeNumerosTests {
        [Fact]
        public void DeveGerarExcecaoQuandoNumeroInicialForMenorQueZero() {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
                new ProcessamentoDeNumeros(-1, 10)
            );
        }

        [Fact]
        public void DeveGerarExcecaoQuandoNumeroFinalForMaiorQueCem() {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
                new ProcessamentoDeNumeros(10, 101)
            );
        }

        [Fact]
        public void DeveGerarExcecaoQuandoNumeroInicialForMaiorQueNumeroFinal() {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
                new ProcessamentoDeNumeros(10, 5)
            );
        }

        [Fact]
        public void DeveGerar21LetrasQuandoNumerosForem1E5() {

            var quantidadeDeLetrasEsperadas = 21;

            var processamentoDeNumeros = new ProcessamentoDeNumeros(1, 5);
            
            Assert.Equal(quantidadeDeLetrasEsperadas, processamentoDeNumeros.QuantidadeDeLetrasTotais);
        }

        [Fact]
        public void DeveGerar21LetrasQuandoNumerosForem0E0() {

            var quantidadeDeLetrasEsperadas = 4;

            var processamentoDeNumeros = new ProcessamentoDeNumeros(0, 0);
            
            Assert.Equal(quantidadeDeLetrasEsperadas, processamentoDeNumeros.QuantidadeDeLetrasTotais);
        }

        [Fact]
        public void DeveGerar21LetrasQuandoNumerosForem99E100() {

            var quantidadeDeLetrasEsperadas = 15;

            var processamentoDeNumeros = new ProcessamentoDeNumeros(99, 100);
            
            Assert.Equal(quantidadeDeLetrasEsperadas, processamentoDeNumeros.QuantidadeDeLetrasTotais);
        }

        [Fact]
        public void DeveGerar21LetrasQuandoNumerosForem80E100() {

            var quantidadeDeLetrasEsperadas = 235;

            var processamentoDeNumeros = new ProcessamentoDeNumeros(80, 100);
            
            Assert.Equal(quantidadeDeLetrasEsperadas, processamentoDeNumeros.QuantidadeDeLetrasTotais);
        }
    }
}
