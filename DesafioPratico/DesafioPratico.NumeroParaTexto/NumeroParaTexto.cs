﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesafioPratico.NumeroParaTexto {
    public static class NumberTextExtensionMethod {
        public static string ParaTexto(this int num) {
            var numeroParaTexto = new NumeroParaTexto();
            return numeroParaTexto.ParaTexto(num);
        }
    }

    public class NumeroParaTexto {

        private Dictionary<int, string> numerosPorExtenso = new Dictionary<int, string>();
        private StringBuilder numeroPorExtenso;

        public NumeroParaTexto() {
            Initialize();
        }

        public string ParaTexto(int num) {
            numeroPorExtenso = new StringBuilder();

            if (num == 0) {
                numeroPorExtenso.Append(numerosPorExtenso[num]);
                return numeroPorExtenso.ToString();
            }

            //num = scales.Aggregate(num, (current, scale) => Append(current, scale.Key));
            //AppendLessThanOneThousand(num);
            //num = AppendHundreds(num);
            num = FormatarDezenas(num);
            FormatarUnidades(num);

            return numeroPorExtenso.ToString().Trim();
        }

        private void FormatarUnidades(int num) {
            if (num > 0) {
                numeroPorExtenso.AppendFormat("{0} ", numerosPorExtenso[num]);
            }
        }

        private int FormatarDezenas(int num) {
            if (num > 20) {
                var tens = ((int)(num / 10)) * 10;
                numeroPorExtenso.AppendFormat("{0} ", numerosPorExtenso[tens]);

                if(num != tens)
                    numeroPorExtenso.Append(" e ");
                num = num - tens;
            }
            return num;
        }

        //private int FormatarCentenas(int num) {
        //    if (num > 99) {
        //        var hundreds = ((int)(num / 100));
        //        numeroPorExtenso.AppendFormat("{0} hundred ", numerosPorExtenso[hundreds]);
        //        num = num - (hundreds * 100);
        //    }
        //    return num;
        //}

        private void Initialize() {
            numerosPorExtenso.Add(0, "zero");
            numerosPorExtenso.Add(1, "um");
            numerosPorExtenso.Add(2, "dois");
            numerosPorExtenso.Add(3, "três");
            numerosPorExtenso.Add(4, "quatro");
            numerosPorExtenso.Add(5, "cinco");
            numerosPorExtenso.Add(6, "seis");
            numerosPorExtenso.Add(7, "sete");
            numerosPorExtenso.Add(8, "oito");
            numerosPorExtenso.Add(9, "nove");
            numerosPorExtenso.Add(10, "dez");
            numerosPorExtenso.Add(11, "onze");
            numerosPorExtenso.Add(12, "doze");
            numerosPorExtenso.Add(13, "treze");
            numerosPorExtenso.Add(14, "quatorze");
            numerosPorExtenso.Add(15, "quinze");
            numerosPorExtenso.Add(16, "dezesseis");
            numerosPorExtenso.Add(17, "dezesete");
            numerosPorExtenso.Add(18, "dezoito");
            numerosPorExtenso.Add(19, "dezenove");
            numerosPorExtenso.Add(20, "vinte");
            numerosPorExtenso.Add(30, "trinta");
            numerosPorExtenso.Add(40, "quarenta");
            numerosPorExtenso.Add(50, "cinquenta");
            numerosPorExtenso.Add(60, "sessenta");
            numerosPorExtenso.Add(70, "setenta");
            numerosPorExtenso.Add(80, "oitenta");
            numerosPorExtenso.Add(90, "noventa");
            numerosPorExtenso.Add(100, "cem");
        }
    }
}
