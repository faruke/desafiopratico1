﻿using DesafioPratico.NumeroParaTexto;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesafioPratico.NumeroParaTexto {
    public class ProcessamentoDeNumeros {
        private int numeroInicial;
        private int numeroFinal;
        private List<String> NumerosPorExtenso { get; set; }
        private List<Int32> Numeros { get; set; }

        public ProcessamentoDeNumeros(int numeroInicial, int numeroFinal) {

            if (numeroInicial < 0) throw new ArgumentOutOfRangeException("O número inicial não pode ser menor que zero");

            if (numeroFinal > 100) throw new ArgumentOutOfRangeException("O número final não pode ser maior que cem");

            if (numeroFinal < numeroInicial) throw new ArgumentOutOfRangeException("O número final não pode ser menor que o número inicial");

            this.numeroInicial = numeroInicial;
            this.numeroFinal = numeroFinal;
            this.NumerosPorExtenso = new List<string>();
            this.Numeros = new List<Int32>();
            this.Processar();
        }        

        private void Processar() {
            for (int numero = numeroInicial; numero <= numeroFinal; numero++) {
                this.NumerosPorExtenso.Add(numero.ParaTexto());
                this.Numeros.Add(numero);
            }
        }

        public String NumerosPorExtensoUtilizados {
            get {
                return String.Join(" ", this.NumerosPorExtenso.ToArray());
            }
        }

        public String NumerosUtilizados {
            get {
                return String.Join(" + ", this.Numeros.ToArray());
            }
        }

        public Int32 QuantidadeDeLetrasTotais {
            get {
                return this.NumerosPorExtenso.Select(a => a.Where(c => char.IsLetter(c)).Count()).Sum();
            }
        }
    }
}
