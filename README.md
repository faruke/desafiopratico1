# Desafio Prático 1

## Considerações Gerais

* A solução foi disponibilizada utilizando dotnet core. Foram criados 3 projetos, um console conforme solicitado, uma library para que fosse isolado o processamento e um último projeto para testes unitários utilizando xUnit.

* Do texto assumi algumas premissas:
    * Números negativos não serão aceitos
    * Os espaços entre as palavras não são considerados letras, logo não são contabilizados
